/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

//==============================================================================
VibrattoAudioProcessor::VibrattoAudioProcessor()
:
#ifndef JucePlugin_PreferredChannelConfigurations
     AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
    state(*this, nullptr, "VibrattoAudioProcessor", dsteam::createVibrattoParameterLayout()),
    params(&vibratto, &state)
{
}

VibrattoAudioProcessor::~VibrattoAudioProcessor()
{
}

//==============================================================================
const String VibrattoAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool VibrattoAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool VibrattoAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool VibrattoAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double VibrattoAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int VibrattoAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int VibrattoAudioProcessor::getCurrentProgram()
{
    return 0;
}

void VibrattoAudioProcessor::setCurrentProgram (int index)
{
}

const String VibrattoAudioProcessor::getProgramName (int index)
{
    return {};
}

void VibrattoAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void VibrattoAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    juce::dsp::ProcessSpec spec;
    
    spec.maximumBlockSize = samplesPerBlock;
    spec.sampleRate = sampleRate;
    spec.numChannels = getTotalNumOutputChannels();
    
	vibratto.prepare(spec);
    params.prepareToPlay(sampleRate, samplesPerBlock);
}

void VibrattoAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool VibrattoAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;
   #endif
    
    if (layouts.getMainInputChannelSet() != AudioChannelSet::mono()
        && layouts.getMainInputChannelSet() != AudioChannelSet::stereo())
        return false;

    return true;
}
#endif

void VibrattoAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    juce::dsp::AudioBlock<float> block { buffer };
    juce::dsp::ProcessContextReplacing<float> context { block };

	vibratto.process(context);
}

//==============================================================================
bool VibrattoAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* VibrattoAudioProcessor::createEditor()
{
	return new GenericAudioProcessorEditor(*this);
}

//==============================================================================
void VibrattoAudioProcessor::getStateInformation (MemoryBlock& destData)
{
   	auto s = state.copyState();
	std::unique_ptr<XmlElement> xml(s.createXml());
	copyXmlToBinary(*xml, destData);
}

void VibrattoAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState.get() != nullptr)
		if (xmlState->hasTagName(state.state.getType()))
			state.replaceState(ValueTree::fromXml(*xmlState));
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new VibrattoAudioProcessor();
}
