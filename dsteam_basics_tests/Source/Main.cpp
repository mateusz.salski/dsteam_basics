/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include <iostream>
#include <cmath>

//==============================================================================
int main (int argc, char* argv[])
{

	auto result = dsteam::beatInMs(140);
	std::cout << result << std::endl;

	result = dsteam::beatInSamples(44100., 4);

	result = dsteam::linearInterpolation(0, 1, 1, 2);

	result = dsteam::millis2samples(44100., 0.4f);

	int i;
	std::cin >> i;

    return 0;
}
