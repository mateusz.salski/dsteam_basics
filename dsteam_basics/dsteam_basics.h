/** BEGIN_JUCE_MODULE_DECLARATION

    ID:            dsteam_basics
    vendor:         Mateusz Salski
    version:        0.0.1
    name:          Digital Steam - Simple and common functions and classes.
    description:     A set of function and classes coded while leaning JUCE and DSP.
    website:        https://digitalsteam.pl
    license:        ISC

    dependencies:   juce_core,juce_audio_basics,juce_audio_processors,juce_dsp

    END_JUCE_MODULE_DECLARATION
*/

#pragma once
#define JUCE_AUDIO_PROCESSORS_H_INCLUDED

#ifndef __DSTEAM_BASICS_H_INCLUDED
#define __DSTEAM_BASICS_H_INCLUDED

#include <juce_core/juce_core.h>
#include <juce_audio_basics/juce_audio_basics.h>
#include <juce_audio_processors/juce_audio_processors.h>
#include <juce_dsp/juce_dsp.h>

#if ! DONT_SET_USING_JUCE_NAMESPACE
// If your code uses a lot of JUCE classes, then this will obviously save you
// a lot of typing, but can be disabled by setting DONT_SET_USING_JUCE_NAMESPACE.
using namespace juce;
#endif

namespace dsteam
{
	template <typename T>
	T millis2samples(double sampleRate, T miliseconds)
	{
		return static_cast<T>(sampleRate * static_cast<double>(miliseconds) / 1000.);
	}

	constexpr float samples2millis(double sampleRate, float samples);

	float linearInterpolation(float x1, float y1, float x2, float y2);

	double beatInSamples(double sampleRate, double bpm);

	double beatInMs(double bpm);

	String percentValueToTextFunction(float value, int /*maxLen*/);

	float percentTextToValueFunction(const String& text);

	String dbValueToTextFunction(float value, int /*maxLen*/);

	float dbTextToValueFunction(const String& text);

	String linearValueToDbTextFunction(float value, int /*maxLen*/);

	float dbTextToLinearValueFunction(const String& text);
}


#include "utils/dsteam_CircularType.h"
#include "utils/dsteam_LinearInterpolator.h"

#include "audio_processors/dsteam_ProcessorWrapper.h"
#include "audio_processors/dsteam_StatefulAudioProcessor.h"
#include "audio_processors/dsteam_GainAudioProcessor.h"
#include "audio_processors/dsteam_AudioProcessorBase.h"
#include "audio_processors/dsteam_Chorus.h"
#include "audio_processors/dsteam_ChorusParameters.h"
#include "audio_processors/dsteam_Vibratto.h"
#include "audio_processors/dsteam_VibrattoParameters.h"

#endif
