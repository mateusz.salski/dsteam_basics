#include "dsteam_basics.h"

#include "utils/dsteam_HelperFunctions.cpp"
#include "audio_processors/dsteam_Chorus.cpp"
#include "audio_processors/dsteam_ChorusParameters.cpp"
#include "audio_processors/dsteam_VibrattoParameters.cpp"