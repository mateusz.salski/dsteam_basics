/*
  ==============================================================================

    ChorusParameters.cpp
    Created: 27 Nov 2018 8:09:43pm
    Author:  Mateusz

  ==============================================================================
*/

namespace dsteam
{
	AudioProcessorValueTreeState::ParameterLayout createChorusParameterLayout()
	{
		std::vector<std::unique_ptr<RangedAudioParameter>> params;

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusSpeed", "Speed",
			NormalisableRange<float>(0.f, 5.f), 2.f, "Hz"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusSweepWidth", "SweepWidth",
			NormalisableRange<float>(0.f, dsp::ChorusConsts::maxSweepWidthInMs), 0.f, "ms"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusDepth", "Depth",
			NormalisableRange<float>(0.f, 1.f), .5f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusDelay", "Delay",
			NormalisableRange<float>(0.f, dsp::ChorusConsts::maxDelayInMs), 0.f, "ms"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusFeedback", "Feedback",
			NormalisableRange<float>(0.f, 1.f), 0.f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusOriginal", "Original",
			NormalisableRange<float>(0.f, 1.f), 0.f, ""));

		return { params.begin(), params.end() };
	}

	ChorusParameters::ChorusParameters(dsp::MonoChorus::State* chorusState, AudioProcessorValueTreeState* state)
		: chorusState(chorusState)
	{
		chorusDepth = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusDepth"));
		chorusDepth->addListener(this);

		chorusOriginal = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusOriginal"));
		chorusOriginal->addListener(this);

		chorusSpeed = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusSpeed"));
		chorusSpeed->addListener(this);

		chorusSweepWidth = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusSweepWidth"));
		chorusSweepWidth->addListener(this);

		chorusDelay = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusDelay"));
		chorusDelay->addListener(this);

		chorusFeedback = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusFeedback"));
		chorusFeedback->addListener(this);
	}

	void ChorusParameters::prepareToPlay(double sampleRate, int)
	{
		chorusState->sampleRate = sampleRate;
		chorusState->setDepth(chorusDepth->get());
		chorusState->setOriginal(chorusOriginal->get());
		chorusState->setSpeed(chorusSpeed->get());
		chorusState->setSweepWidth(chorusSweepWidth->get());
		chorusState->setDelay(chorusDelay->get());
		chorusState->setFeedback(chorusFeedback->get());

		isPrepared = true;
	}

	void ChorusParameters::releaseResources()
	{
		isPrepared = false;
	}

	void ChorusParameters::parameterValueChanged(int parameterIndex, float /*newValue*/)
	{
		if (!isPrepared)
			return;

		if (parameterIndex == chorusSpeed->getParameterIndex())
			chorusState->setSpeed(chorusSpeed->get());
		else if (parameterIndex == chorusSweepWidth->getParameterIndex())
			chorusState->setSweepWidth(chorusSweepWidth->get());
		else if (parameterIndex == chorusDepth->getParameterIndex())
			chorusState->setDepth(chorusDepth->get());
		else if (parameterIndex == chorusDelay->getParameterIndex())
			chorusState->setDelay(chorusDelay->get());
		else if (parameterIndex == chorusFeedback->getParameterIndex())
			chorusState->setFeedback(chorusFeedback->get());
		else if (parameterIndex == chorusOriginal->getParameterIndex())
			chorusState->setOriginal(chorusOriginal->get());
	}

	void ChorusParameters::parameterGestureChanged(int, bool)
	{}
} // namespace dsteam