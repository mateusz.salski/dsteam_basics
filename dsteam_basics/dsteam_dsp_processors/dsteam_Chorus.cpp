/*
  ==============================================================================

    MonoChorus.cpp
    Created: 27 Nov 2018 8:09:34pm
    Author:  Mateusz

  ==============================================================================
*/

#include "dsteam_Chorus.h"

namespace dsteam
{
	namespace dsp
	{
		void MonoChorus::prepare(const dsteam::dsp::MonoProcessSpec& spec)
		{
			state->sampleRate = spec.sampleRate;

			state->setDelay(20.f);
			state->setSweepWidth(3.f);
			state->setSpeed(2.5f);

			int delayBuffLenInSmps = static_cast<int>(
				ceil(dsteam::millis2samples(
					state->sampleRate,
					static_cast<float>(state->maxSweepWidthInMs + state->maxDelayInMs)
				)
				));

			delayBuffer.setSize(
				spec.numChannels,
				delayBuffLenInSmps,
				false, true, true
			);

			phase = (float)spec.channelId / (float)spec.duplicatorNumChannels;
		}

		void MonoChorus::proccessOneChannel(int ch, const juce::dsp::AudioBlock<float>& inBlock, juce::dsp::AudioBlock<float>& outBlock, int i)
		{
			// LFO
			//float currentDelay = state->delayInSamples + state->sweepWidthInSamples *
			//	(0.5f + 0.5f * sin(MathConstants<float>::twoPi * phase)); // 49 2,02%

			float currentDelay = state->delayInSamples + state->sweepWidthInSamples *
				(0.5f + 0.5f * juce::dsp::FastMathApproximations::sin<float>(MathConstants<float>::pi * phase)); 

			phase += state->speedInSmp;
			if (phase >= 1.0)
				phase -= 2.0;

			// move play head
			float delayPlayHeadIndex = fmodf(
				(float)delayRecordHeadIndex
				- (float)currentDelay
				+ (float)delayBuffer.getNumSamples(),
				(float)delayBuffer.getNumSamples()
			);  // 59 2,65%

			// read sample from delay buffer - interpolate sample
			float fraction = delayPlayHeadIndex - floor(delayPlayHeadIndex);  // 36 1,52%
			int prevIdx = (int)floor(delayPlayHeadIndex);
			int nextIdx = (prevIdx + 1) % delayBuffer.getNumSamples();

			float interpolatedSample =
				fraction * delayBuffer.getReadPointer(ch)[nextIdx]
				+ (1.f - fraction) * delayBuffer.getReadPointer(ch)[prevIdx];

			// update delay buffer
			auto in = inBlock.getChannelPointer(ch);
			delayBuffer.getWritePointer(ch)[delayRecordHeadIndex] =
				in[i] + interpolatedSample * state->feedback;

			// combine output sample
			auto out = outBlock.getChannelPointer(ch);
			out[i] = state->original * in[i] + state->depth * interpolatedSample;

		}
	} // namespace dsp
} // namespace dsteam