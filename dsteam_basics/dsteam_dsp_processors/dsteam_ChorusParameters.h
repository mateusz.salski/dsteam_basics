/*
  ==============================================================================

    ChorusParameters.h
    Created: 27 Nov 2018 8:09:43pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

#include "dsteam_Chorus.h"

namespace dsteam
{
	AudioProcessorValueTreeState::ParameterLayout createChorusParameterLayout();

	class ChorusParameters
		: public AudioProcessorParameter::Listener
	{
	public:
		ChorusParameters(dsp::MonoChorus::State* chorusState, AudioProcessorValueTreeState* state);

		void prepareToPlay(double sampleRate, int samplesPerBlock);
		void releaseResources();

		void parameterValueChanged(int parameterIndex, float newValue) override;
		void parameterGestureChanged(int parameterIndex, bool gestureIsStarting) override;

	private:
		dsp::MonoChorus::State * chorusState;

		AudioParameterFloat* chorusSpeed;
		AudioParameterFloat* chorusSweepWidth;
		AudioParameterFloat* chorusDelay;
		AudioParameterFloat* chorusDepth;
		AudioParameterFloat* chorusFeedback;
		AudioParameterFloat* chorusOriginal;

		bool isPrepared = false;
	};
} // namespace dsteam