/*
  ==============================================================================

    MonoChorus.h
    Created: 27 Nov 2018 8:09:34pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	namespace dsp
	{
		/*
		Chorus:
		- Depth (feedback amount)
		- Delay <20, 30> ms
		- Sweep width > 5ms
		- Speed < 3Hz
		- Waveform: sine
		- Number of voices
		*/
		namespace ChorusConsts
		{
			constexpr int maxDelayInMs = 30;
			constexpr int maxSweepWidthInMs = 30;
		}

		class MonoChorus 
		{
		public:
			struct State : public juce::dsp::ProcessorState
			{
				using Ptr = ReferenceCountedObjectPtr<MonoChorus::State>;

				void setDepth(float newDepth)
				{
					depth = newDepth;
				}

				void setDelay(float delayInMs)
				{
					jassert(sampleRate > 0);
					jassert(delayInMs <= maxDelayInMs);

					delayInSamples = dsteam::millis2samples(
						sampleRate,
						delayInMs
					);
				}

				void setSweepWidth(float sweepWidthInMs)
				{
					jassert(sampleRate > 0);
					jassert(sweepWidthInMs <= maxSweepWidthInMs);

					sweepWidthInSamples = millis2samples(
						sampleRate,
						sweepWidthInMs
					);
				}

				void setSpeed(float speedInHz)
				{
					jassert(sampleRate > 0);
					speedInSmp = speedInHz / static_cast<float>(sampleRate);
				}

				void setFeedback(float newFeedback)
				{
					feedback = newFeedback;
				}

				void setOriginal(float newOriginal)
				{
					original = newOriginal;
				}

				float sweepWidthInSamples = 0.f;
				float delayInSamples = 0.f;
				float speedInSmp;
				float depth = 1.0f;
				float feedback = 0.1f;
				float original = 1.f;

				double sampleRate = -1.;

				const int maxDelayInMs = ChorusConsts::maxDelayInMs;
				const int maxSweepWidthInMs = ChorusConsts::maxSweepWidthInMs;

			};

			MonoChorus(MonoChorus::State::Ptr state) 
				: state(std::move(state)) 
			{ 
				reset(); 
			}

			void prepare(const dsteam::dsp::MonoProcessSpec& spec) ;

			template <class ProcessContext>
			void process(const dsteam::dsp::ProcessorDuplicator
				<MonoChorus, MonoChorus::State>
				::MonoProcessContext<ProcessContext>& context)
			{
				const ProcessContext::AudioBlockType inBlock = context.getInputBlock();
				ProcessContext::AudioBlockType outBlock = context.getOutputBlock();

				if (context.isBypassed)
				{
					if (context.usesSeparateInputAndOutputBlocks())
						context.getOutputBlock().copy(context.getInputBlock());

					return;
				}

				for (int sampleIndex = 0; sampleIndex < inBlock.getNumSamples(); ++sampleIndex)
				{
					proccessOneChannel(0, inBlock, outBlock, sampleIndex);

					if (++delayRecordHeadIndex >= delayBuffer.getNumSamples())
						delayRecordHeadIndex = 0;
				}
			}

			void reset() { }
		private:
			MonoChorus::State::Ptr state = nullptr;

			float phase = -1.f;

			AudioBuffer<float> delayBuffer;
			int delayRecordHeadIndex = 0;

			void proccessOneChannel(int ch, const juce::dsp::AudioBlock<float>& inBlock, juce::dsp::AudioBlock<float>& outBlock, int i);
		};
	} // namespace ms::dsp

	//using ChorusAudioProcessor = ProcessorWrapper<dsp::Chorus>;

} // namespace dsteam