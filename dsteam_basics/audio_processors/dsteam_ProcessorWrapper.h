#include "dsteam_AudioProcessorBase.h"

namespace dsteam
{
	template <class DspProcessor>
	struct ProcessorWrapper :
		public AudioProcessorBase,
		public DspProcessor
	{
		void prepareToPlay(double newSampleRate, int samplesPerBlock) override
		{
			const juce::dsp::ProcessSpec spec{ 
				newSampleRate,
				static_cast<juce::uint32>(samplesPerBlock), 
				static_cast<juce::uint32>(getTotalNumOutputChannels())
			};
            DspProcessor::prepare(spec);
		}

		void releaseResources() override
		{
			DspProcessor::reset();
		}

		void processBlock(AudioSampleBuffer& buffer, MidiBuffer&) override
		{
			juce::dsp::AudioBlock<float> block(buffer);
			juce::dsp::ProcessContextReplacing<float> context(block);
            DspProcessor::process(context);
		}
	};
}
