/*
  ==============================================================================

    GainAudioProcessor.h
    Created: 20 Nov 2018 12:43:56pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	using GainAudioProcessor = ProcessorWrapper<juce::dsp::Gain<float>>;
}