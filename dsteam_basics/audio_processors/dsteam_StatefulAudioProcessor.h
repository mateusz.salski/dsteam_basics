namespace dsteam
{
	class StatefulAudioProcessor : public AudioProcessor
	{
	public:
		StatefulAudioProcessor(const Identifier& valueTreeType, AudioProcessorValueTreeState::ParameterLayout parameterLayout)
			:
#ifndef JucePlugin_PreferredChannelConfigurations
			AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
				.withInput("Input", AudioChannelSet::stereo(), true)
#endif
				.withOutput("Output", AudioChannelSet::stereo(), true)
#endif
			),
#endif
        state(*this, nullptr, valueTreeType, std::move(parameterLayout))
		{}

		//==============================================================================
		void getStateInformation(MemoryBlock& destData) override
		{
			auto s = state.copyState();
			std::unique_ptr<XmlElement> xml(s.createXml());
			copyXmlToBinary(*xml, destData);
		}

		void setStateInformation(const void* data, int sizeInBytes) override
		{
			std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
			if (xmlState.get() != nullptr)
				if (xmlState->hasTagName(state.state.getType()))
					state.replaceState(ValueTree::fromXml(*xmlState));
		}
	protected:
		AudioProcessorValueTreeState state;
	};
} // namespace dsteam
