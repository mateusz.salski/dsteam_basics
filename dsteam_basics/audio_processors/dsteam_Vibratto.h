/*
  ==============================================================================

    Vibratto.h
    Created: 22 Nov 2018 11:35:07am
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	namespace dsp
	{
		/*

		Vibratto:
		- no feedback
		- Speed: lfo frq: 6Hz
		- Waveform:
		- Sweep width: lfo width: ~0.1% <-> +- 0.265 ms

		Chorous:
		- Depth (feedback amount)
		- Delay <20, 30> ms
		- Sweep width > 5ms
		- Speed < 3Hz
		- Waveform: sine
		- Number of voices

		Flanger: delay <1, 10> ms, with feedback (depth)
		- Stereo? different phase on channels
		- Depth (mix)
		- Delay: >100ms ?
		- Sweep width: <5ms
		- Speed: <0.1, 10> Hz
		- Waveform
		-Feedback (regeneration)
		- Inverted mode (Phase)
		*/

		class Vibratto :
			public juce::dsp::ProcessorBase
		{
		public:
			Vibratto()
			{
				chorus.setFeedback(0.f);
				chorus.setOriginal(0.f);
				chorus.setDepth(1.f);
			}

			void prepare(const juce::dsp::ProcessSpec& spec) override
			{
				chorus.prepare(spec);

				chorus.setDelay(0.f);
				chorus.setSweepWidth(.5f);
				chorus.setSpeed(3.f);
			}

			void process(const juce::dsp::ProcessContextReplacing<float>& context) override
			{
				chorus.process(context);
			}

			void reset() override
			{
				chorus.reset();
			}

			void setDepth(float newDepth)
			{
				auto newWet = newDepth >= 0.5f ? 1.f : 2.f * newDepth;
				auto newOrginal = 1.f - newWet;

				chorus.setDepth(newWet);
				chorus.setOriginal(newOrginal);
			}

			void setSpeed(float newSpeed)
			{
				chorus.setSpeed(newSpeed);
			}

			void setSweepWidth(float newSweepWidth)
			{
				chorus.setSweepWidth(newSweepWidth);
			}


		private:
			Chorus chorus;
		};
	} // namespace dsp

} // namespace dsteam
