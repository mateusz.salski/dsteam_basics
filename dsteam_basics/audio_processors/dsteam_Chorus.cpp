/*
  ==============================================================================

    Chorus.cpp
    Created: 27 Nov 2018 8:09:34pm
    Author:  Mateusz

  ==============================================================================
*/

namespace dsteam
{
	namespace dsp
	{
		void Chorus::prepare(const juce::dsp::ProcessSpec& spec)
		{
			sampleRate = spec.sampleRate;

			setDelay(20.f);
			setSweepWidth(3.f);
			setSpeed(2.5f);

			int delayBuffLenInSmps = static_cast<int>(
				ceil(dsteam::millis2samples(
					sampleRate,
					static_cast<float>(maxSweepWidthInMs + maxDelayInMs)
				)
				));

			delayBuffer.setSize(
				spec.numChannels,
				delayBuffLenInSmps,
				false, true, true
			);

			phases.resize(spec.numChannels);
			for (juce::uint32 ch = 0; ch < spec.numChannels; ++ch)
				phases[ch] = (float) ch / (float) spec.numChannels;
		}

		void Chorus::process(const juce::dsp::ProcessContextReplacing<float>& context)
		{
			if (context.isBypassed)
			{
				if (context.usesSeparateInputAndOutputBlocks())
					context.getOutputBlock().copyFrom(context.getInputBlock());
			}
			else
			{
				auto& inBlock = context.getInputBlock();
				auto& outBlock = context.getOutputBlock();

				for (int i = 0; i < inBlock.getNumSamples(); ++i)
				{
					for (auto channel = 0; channel < jmin(outBlock.getNumChannels(), inBlock.getNumChannels()); ++channel)
						proccessOneChannel(channel, inBlock, outBlock, i);

					if (++delayRecordHeadIndex >= delayBuffer.getNumSamples())
						delayRecordHeadIndex = 0;
				}
			}
		}

		void Chorus::proccessOneChannel(int ch, const juce::dsp::AudioBlock<const float>& inBlock, juce::dsp::AudioBlock<float>& outBlock, int i)
		{
			// LFO
			float currentDelay = delayInSamples + sweepWidthInSamples *
				(0.5f + 0.5f * sin(MathConstants<float>::twoPi * phases[ch]));

			phases[ch] += speedInSmp;
			if (phases[ch] >= 1.0)
				phases[ch] = -1.0;

			// move play head
			float delayPlayHeadIndex = fmodf(
				(float)delayRecordHeadIndex
				- (float)currentDelay
				+ (float)delayBuffer.getNumSamples(),
				(float)delayBuffer.getNumSamples()
			);

			// read sample from delay buffer - interpolate sample
			float fraction = delayPlayHeadIndex - floor(delayPlayHeadIndex);
			int prevIdx = (int)floor(delayPlayHeadIndex);
			int nextIdx = (prevIdx + 1) % delayBuffer.getNumSamples();

			float interpolatedSample =
				fraction * delayBuffer.getReadPointer(ch)[nextIdx]
				+ (1.f - fraction) * delayBuffer.getReadPointer(ch)[prevIdx];

			// update delay buffer
			auto in = inBlock.getChannelPointer(ch);
			delayBuffer.getWritePointer(ch)[delayRecordHeadIndex] =
				in[i] + interpolatedSample * feedback;

			// combine output sample
			auto out = outBlock.getChannelPointer(ch);
			out[i] = original * in[i] + depth * interpolatedSample;

		}
	} // namespace dsp
} // namespace dsteam
