/*
  ==============================================================================

    Chorus.h
    Created: 27 Nov 2018 8:09:34pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	namespace dsp
	{
		/*
		Chorus:
		- Depth (feedback amount)
		- Delay <20, 30> ms
		- Sweep width > 5ms
		- Speed < 3Hz
		- Waveform: sine
		- Number of voices
		*/
		namespace ChorusConsts
		{
			const int maxDelayInMs = 30;
			const int maxSweepWidthInMs = 30;
		}

		class Chorus : public juce::dsp::ProcessorBase
		{
		public:
			void prepare(const juce::dsp::ProcessSpec& spec);
			void process(const juce::dsp::ProcessContextReplacing<float>& context);
			void reset() {}

			void setDepth(float newDepth)
			{
				depth = newDepth;
			}

			void setDelay(float delayInMs)
			{
				jassert(sampleRate > 0);
				jassert(delayInMs <= maxDelayInMs);

				delayInSamples = dsteam::millis2samples(
					sampleRate,
					delayInMs
				);
			}

			void setSweepWidth(float sweepWidthInMs)
			{
				jassert(sampleRate > 0);
				jassert(sweepWidthInMs <= maxSweepWidthInMs);

				sweepWidthInSamples = millis2samples(
					sampleRate,
					sweepWidthInMs
				);
			}

			void setSpeed(float speedInHz)
			{
				jassert(sampleRate > 0);
				speedInSmp = speedInHz / static_cast<float>(sampleRate);
			}

			void setFeedback(float newFeedback)
			{
				feedback = newFeedback;
			}

			void setOriginal(float newOriginal)
			{
				original = newOriginal;
			}
		private:
			float sweepWidthInSamples = 0.f;
			float delayInSamples = 0.f;
			float speedInSmp;
			float depth = 1.0f;
			float feedback = 0.1f;
			float original = 1.f;

			std::vector<float> phases;

			AudioBuffer<float> delayBuffer;
			int delayRecordHeadIndex = 0;

			int maxDelayInMs = ChorusConsts::maxDelayInMs;
			int maxSweepWidthInMs = ChorusConsts::maxSweepWidthInMs;
			double sampleRate = -1;

			void proccessOneChannel(int ch, const juce::dsp::AudioBlock<const float>& inBlock, juce::dsp::AudioBlock<float>& outBlock, int i);
		};
	} // namespace dsteam::dsp

} // namespace dsteam
