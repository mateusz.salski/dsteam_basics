/*
  ==============================================================================

    ProcessorBase.h
    Created: 20 Nov 2018 10:09:22am
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	class AudioProcessorBase : public AudioProcessor
	{
	public:
		//==============================================================================
		AudioProcessorBase() {}
		AudioProcessorBase(const BusesProperties& ioLayouts) : AudioProcessor(ioLayouts) {}
		~AudioProcessorBase() {}
		//==============================================================================
		void prepareToPlay(double, int) override {}
		void releaseResources() override {}
		void processBlock(AudioSampleBuffer& /*buffer*/, MidiBuffer&) override {}
		//==============================================================================
		AudioProcessorEditor* createEditor() override { return nullptr; }
		bool hasEditor() const override { return false; }
		//==============================================================================
		const String getName() const override { return {}; }
		bool acceptsMidi() const override { return false; }
		bool producesMidi() const override { return false; }
		double getTailLengthSeconds() const override { return 0; }
		//==============================================================================
		int getNumPrograms() override { return 0; }
		int getCurrentProgram() override { return 0; }
		void setCurrentProgram(int) override {}
		const String getProgramName(int) override { return {}; }
		void changeProgramName(int, const String&) override {}
		//==============================================================================
		void getStateInformation(MemoryBlock& /*destData*/) override {}
		void setStateInformation(const void* /*data*/, int /*sizeInBytes*/) override {}
	private:
		//==============================================================================
		JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioProcessorBase)
	};
} // namespace dsteam