/*
  ==============================================================================

    ChorusParameters.cpp
    Created: 27 Nov 2018 8:09:43pm
    Author:  Mateusz

  ==============================================================================
*/

namespace dsteam
{
	AudioProcessorValueTreeState::ParameterLayout createChorusParameterLayout()
	{
		std::vector<std::unique_ptr<RangedAudioParameter>> params;

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusSpeed", "chorusSpeed",
			NormalisableRange<float>(0.f, 5.f), 2.f, "Hz"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusSweepWidth", "chorusSweepWidth",
			NormalisableRange<float>(0.f, dsp::ChorusConsts::maxSweepWidthInMs), 0.f, "ms"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusDepth", "chorusDepth",
			NormalisableRange<float>(0.f, 1.f), .5f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusDelay", "chorusDelay",
			NormalisableRange<float>(0.f, dsp::ChorusConsts::maxDelayInMs), 0.f, "ms"));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusFeedback", "chorusFeedback",
			NormalisableRange<float>(0.f, 1.f), 0.f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"chorusOriginal", "chorusOriginal",
			NormalisableRange<float>(0.f, 1.f), 0.f, ""));

		return { params.begin(), params.end() };
	}

    ChorusParameters::ChorusParameters(dsteam::dsp::Chorus* chorusProcessor, AudioProcessorValueTreeState* state)
		: chorusProcessor(chorusProcessor)
	{
		chorusDepth = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusDepth"));
		chorusDepth->addListener(this);

		chorusOriginal = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusOriginal"));
		chorusOriginal->addListener(this);

		chorusSpeed = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusSpeed"));
		chorusSpeed->addListener(this);

		chorusSweepWidth = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusSweepWidth"));
		chorusSweepWidth->addListener(this);

		chorusDelay = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusDelay"));
		chorusDelay->addListener(this);

		chorusFeedback = dynamic_cast<AudioParameterFloat*> (state->getParameter("chorusFeedback"));
		chorusFeedback->addListener(this);
	}

	void ChorusParameters::prepareToPlay(double, int)
	{
		chorusProcessor->setDepth(chorusDepth->get());
		chorusProcessor->setOriginal(chorusOriginal->get());
		chorusProcessor->setSpeed(chorusSpeed->get());
		chorusProcessor->setSweepWidth(chorusSweepWidth->get());
		chorusProcessor->setDelay(chorusDelay->get());
		chorusProcessor->setFeedback(chorusFeedback->get());

		isPrepared = true;
	}

	void ChorusParameters::releaseResources()
	{
		isPrepared = false;
	}

	void ChorusParameters::parameterValueChanged(int parameterIndex, float /*newValue*/)
	{
		if (!isPrepared)
			return;

		if (parameterIndex == chorusSpeed->getParameterIndex())
			chorusProcessor->setSpeed(chorusSpeed->get());
		else if (parameterIndex == chorusSweepWidth->getParameterIndex())
			chorusProcessor->setSweepWidth(chorusSweepWidth->get());
		else if (parameterIndex == chorusDepth->getParameterIndex())
			chorusProcessor->setDepth(chorusDepth->get());
		else if (parameterIndex == chorusDelay->getParameterIndex())
			chorusProcessor->setDelay(chorusDelay->get());
		else if (parameterIndex == chorusFeedback->getParameterIndex())
			chorusProcessor->setFeedback(chorusFeedback->get());
		else if (parameterIndex == chorusOriginal->getParameterIndex())
			chorusProcessor->setOriginal(chorusOriginal->get());
	}

	void ChorusParameters::parameterGestureChanged(int, bool)
	{}
} // namespace dsteam
