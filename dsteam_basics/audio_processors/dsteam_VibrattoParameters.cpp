
namespace dsteam
{
	void createVibrattoParameters(std::vector<std::unique_ptr<RangedAudioParameter>>& params)
	{
		params.push_back(std::make_unique<AudioParameterFloat>(
			"vibrattoDepth", "vibrattoDepth",
			NormalisableRange<float>(0.f, 1.f), 1.f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"vibrattoSpeed", "vibrattoSpeed",
			NormalisableRange<float>(0.f, 6.f), 3.f, ""));

		params.push_back(std::make_unique<AudioParameterFloat>(
			"vibrattoSweepWidth", "vibrattoSweepWidth",
			NormalisableRange<float>(0.f, .265f), .265f, ""));
	}

	AudioProcessorValueTreeState::ParameterLayout createVibrattoParameterLayout()
	{
		std::vector<std::unique_ptr<RangedAudioParameter>> params;
		createVibrattoParameters(params);
		return { params.begin(), params.end() };
	}
} // namespace dsteam