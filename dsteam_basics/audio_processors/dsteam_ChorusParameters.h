/*
  ==============================================================================

    ChorusParameters.h
    Created: 27 Nov 2018 8:09:43pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

namespace dsteam
{
	AudioProcessorValueTreeState::ParameterLayout createChorusParameterLayout();

	class ChorusParameters
		: public AudioProcessorParameter::Listener
	{
	public:
        ChorusParameters(dsteam::dsp::Chorus* chorusProcessor, AudioProcessorValueTreeState* state);

		void prepareToPlay(double sampleRate, int samplesPerBlock);
		void releaseResources();

		void parameterValueChanged(int parameterIndex, float newValue) override;
		void parameterGestureChanged(int parameterIndex, bool gestureIsStarting) override;

	private:
        dsteam::dsp::Chorus * chorusProcessor;

		AudioParameterFloat* chorusSpeed;
		AudioParameterFloat* chorusSweepWidth;
		AudioParameterFloat* chorusDelay;
		AudioParameterFloat* chorusDepth;
		AudioParameterFloat* chorusFeedback;
		AudioParameterFloat* chorusOriginal;

		bool isPrepared = false;
	};
} // namespace dsteam
