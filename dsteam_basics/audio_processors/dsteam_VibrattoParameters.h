#pragma once

namespace dsteam
{
	void createVibrattoParameters(std::vector<std::unique_ptr<RangedAudioParameter>>&);

	AudioProcessorValueTreeState::ParameterLayout createVibrattoParameterLayout();

	class VibrattoParameters
		: public AudioProcessorParameter::Listener
	{
	public:
        VibrattoParameters(dsteam::dsp::Vibratto* vibratto, AudioProcessorValueTreeState* state)
			: vibratto(vibratto)
		{
			vibrattoDepth = dynamic_cast<AudioParameterFloat*> (state->getParameter("vibrattoDepth"));
			vibrattoDepth->addListener(this);

			vibrattoSpeed = dynamic_cast<AudioParameterFloat*> (state->getParameter("vibrattoSpeed"));
			vibrattoSpeed->addListener(this);

			vibrattoSweepWidth = dynamic_cast<AudioParameterFloat*> (state->getParameter("vibrattoSweepWidth"));
			vibrattoSweepWidth->addListener(this);
		}

		void prepareToPlay(double /*sampleRate*/, int /*samplesPerBlock*/)
		{
			vibratto->setSweepWidth(vibrattoSweepWidth->get());
			vibratto->setSpeed(vibrattoSpeed->get());
            
            isPrepared = true;
		}

		void parameterValueChanged(int parameterIndex, float /*newValue*/) override
		{
            if (!isPrepared)
                return;
                
			if (parameterIndex == vibrattoSpeed->getParameterIndex())
				vibratto->setSpeed(vibrattoSpeed->get());
			else if (parameterIndex == vibrattoSweepWidth->getParameterIndex())
				vibratto->setSweepWidth(vibrattoSweepWidth->get());
			else if (parameterIndex == vibrattoDepth->getParameterIndex())
				vibratto->setDepth(vibrattoDepth->get());
		}

		void parameterGestureChanged(int /*parameterIndex*/, bool /*gestureIsStarting*/) override {}

	private:
        dsteam::dsp::Vibratto * vibratto;

		AudioParameterFloat* vibrattoDepth;
		AudioParameterFloat* vibrattoSpeed;
		AudioParameterFloat* vibrattoSweepWidth;
        
        bool isPrepared { false };
	};
} // namespace dsteam
