#pragma once

#ifndef MS_LINEAR_INTERPOLATOR
#define MS_LINEAR_INTERPOLATOR

namespace dsteam
{
	template <typename T>
	class LinearIterpolator
	{
	public:
		void reset()
		{
			init(0);
		}

		void init(T y)
		{
			y1 = y;
		}

		T interpolate(T xdistance, T y0, T x)
		{
			T a = (y1 - y0) / xdistance;
			auto rtn = y1 + a * x;
			y1 = y0;
			return rtn;
		}

	private:
		T y1 = static_cast<T>(0);
	};

} // namespace dsteam

#endif