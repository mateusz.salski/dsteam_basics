#pragma once

namespace dsteam
{
	template <class T>
	inline T wrap(T value, T max)
	{
		return static_cast<T>(std::fmod(value, max));
	}

	/*
	Always positive.
	*/
	template <class T>
	inline T distance(T prev, T next, T max)
	{
		T r;
		if (prev <= next)
			r = next - prev;
		else
			r = max - prev + next;

		return r;
	}

	template <class T>
	inline T circularIncrement(T value, T max)
	{
		return wrap(++value, max);
	}

	template <class T>
	inline T circularAdd(T value, T add, T max)
	{
		return wrap(value + add, max);
	}

	template <class T>
	inline T circularSub(T value, T sub, T max)
	{
		return wrap(max + value - sub, max);
	}

	template <class T>
	inline T revdistance(T prev, T next, T max)
	{
		if (prev >= next)
			return prev - next;
		else
			return prev + max - next;
	}

	inline void addToCircular(juce::dsp::AudioBlock<float> dst, juce::dsp::AudioBlock<float> src, size_t outWritePos, float gain = 1.0f)
	{
		auto numSamplesToWrite = jmin(src.getNumSamples(), (size_t)dst.getNumSamples() - outWritePos);
		dst.getSubBlock(outWritePos, numSamplesToWrite)
			.addProductOf(src.getSubBlock(0, numSamplesToWrite), gain);

		auto numSamplesLeft = src.getNumSamples() - numSamplesToWrite;
		dst.getSubBlock(0, numSamplesLeft)
			.addProductOf(src.getSubBlock(numSamplesToWrite, numSamplesLeft), gain);
	}

	inline void copyToCircular(AudioBuffer<float>& dst, const AudioBuffer<float>& src, int writePos, int numSamples)
	{
		auto numSamplesToWrite = jmin(numSamples, dst.getNumSamples() - writePos);
        
        for (int c = 0; c < jmin(dst.getNumChannels(), src.getNumChannels()); ++c)
        {
            dst.copyFrom(c, writePos, src.getReadPointer(c), numSamplesToWrite);

            auto numSamplesLeft = numSamples - numSamplesToWrite;
            dst.copyFrom(c, 0, src.getReadPointer(c), numSamplesLeft);
        }
	}
    
    inline void copyToCircular(AudioBuffer<float>& dst, const AudioBuffer<float>& src, int writePos)
    {
        copyToCircular(dst, src, writePos, src.getNumSamples());
    }

	template <typename SampleType>
	inline void copyToCircular(juce::dsp::AudioBlock<SampleType>& dst, const juce::dsp::AudioBlock<SampleType>& src, int outWritePos)
	{
		auto numSamplesToWrite = jmin(src.getNumSamples(), (size_t)dst.getNumSamples() - outWritePos);
		dst.getSubBlock(outWritePos, numSamplesToWrite).copy(src.getSubBlock(0, numSamplesToWrite));

		auto numSamplesLeft = src.getNumSamples() - numSamplesToWrite;
		if (numSamplesLeft > 0)
			dst.getSubBlock(0, numSamplesLeft).copy(src.getSubBlock(numSamplesToWrite, numSamplesLeft));
	}

	template <typename SampleType>
	inline void copyFromCircular(juce::dsp::AudioBlock<SampleType>& linearDst, const juce::dsp::AudioBlock<SampleType>& circularSrc, int readPos)
	{
		auto numSamplesToWrite = jmin(linearDst.getNumSamples(), (size_t)circularSrc.getNumSamples() - readPos);
		linearDst.getSubBlock(0, numSamplesToWrite).copyFrom(circularSrc.getSubBlock(readPos, numSamplesToWrite));

		auto numSamplesLeft = linearDst.getNumSamples() - numSamplesToWrite;
		if (numSamplesLeft > 0)
			linearDst.getSubBlock(numSamplesToWrite, numSamplesLeft).copyFrom(circularSrc.getSubBlock(0, numSamplesLeft));
	}

	inline void addToCircular(AudioBuffer<float>& dst, AudioBuffer<float>& src, int writePos, int numSamples)
	{
		auto numSamplesToWrite = jmin(numSamples, dst.getNumSamples() - writePos);
		dst.addFrom(0, writePos, src.getReadPointer(0), numSamplesToWrite);

		auto numSamplesLeft = numSamples - numSamplesToWrite;
		dst.addFrom(0, 0, src.getReadPointer(0), numSamplesLeft);
	}

	inline void multiplyCircular(AudioBuffer<float>& dst, float mul, int writePos, int numSamples)
	{
		auto numSamplesToWrite = jmin(numSamples, dst.getNumSamples() - writePos);
		dst.applyGain(writePos, numSamplesToWrite, mul);

		auto numSamplesLeft = numSamples - numSamplesToWrite;
		dst.applyGain(0, numSamplesLeft, mul);
	}
    
    template <typename SampleType>
    class CircularBuffer
    {
    public:
        juce::AudioBuffer<SampleType> buffer;
  
        void setSize(int maxNumChannels, int maxBufferSize)
        {
            buffer.setSize(maxNumChannels, maxBufferSize);
            buffer.clear();
        }
        
        void copyFrom(const juce::AudioBuffer<SampleType>& src)
        {
            dsteam::copyToCircular(buffer, src, currentPosition);
            currentPosition = dsteam::circularAdd(currentPosition, src.getNumSamples(), buffer.getNumSamples());
        }
        
        void copyTo(juce::AudioBuffer<SampleType>& dst)
        {
            juce::dsp::AudioBlock<SampleType> dstBlock(dst);
            juce::dsp::AudioBlock<SampleType> srcBlock(buffer);
            dsteam::copyFromCircular(dstBlock, srcBlock, currentPosition);
        }
        
    private:
        int currentPosition {0};
    };
    
} // namespace dsteam
