namespace dsteam
{
	constexpr float samples2millis(double sampleRate, float samples)
	{
		return 1000.0f * samples / static_cast<float>(sampleRate);
	}

	float linearInterpolation(float x1, float y1, float x2, float y2)
	{
		float fraction = (x2 - std::floor(x1));
		float sampleFromDelayBuffer = fraction > 0 ? fraction * y2 + (1.f - fraction) * y1 : y1;

		return sampleFromDelayBuffer;
	}

	double beatInSamples(double sampleRate, double bpm)
	{
		return 60.0 / bpm * sampleRate;
	}

	double beatInMs(double bpm)
	{
		return 1000.0 * 60.0 / bpm;
	}

	String percentValueToTextFunction(float value, int /*maxLen*/)
	{
		auto percent = value * 100.f;
		return String(percent);
	}

	float percentTextToValueFunction(const String& text)
	{
		auto f = text.getFloatValue();
		return f / 100.f;
	}

	String dbValueToTextFunction(float value, int /*maxLen*/)
	{ 
		return Decibels::toString(value); 
	}

	float dbTextToValueFunction(const String& text)
	{
		String valueString = text.retainCharacters("-0123456789.");
		return valueString.getFloatValue();
	}

	String linearValueToDbTextFunction(float value, int /*maxLen*/)
	{
		return Decibels::toString(Decibels::gainToDecibels(value));
	}

	float dbTextToLinearValueFunction(const String& text)
	{
		String valueString = text.retainCharacters("-0123456789.");
		return Decibels::decibelsToGain(valueString.getFloatValue());
	}
}